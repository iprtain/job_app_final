#!/bin/python
# -*- coding: utf-8 -*-

import math
import os
import random
import re
import sys

# Realizirati metodu zam
def zam(p):
    numbers = list(p)
    count = 0

    for index in range(len(numbers)):
        smallest_index = index

        for index2 in range(index+1, len(numbers)):
            if numbers[smallest_index] > numbers[index2]:
                smallest_index = index2

        if smallest_index != index:
            count += 1
                
        numbers[index], numbers[smallest_index] = numbers[smallest_index], numbers[index]
        
    return count

if __name__ == '__main__':
    fout = open('izlaz.txt', 'w')
    fin = open('ulaz.txt', 'r')

    n = int(fin.readline())
    p = map(int, fin.readline().rstrip().split())

    x = zam(p)

    fout.write(str(x) + '\n')
    fout.close()
    fin.close()
