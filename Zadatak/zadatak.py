#!/bin/python
# -*- coding: utf-8 -*-

import math
import os
import random
import re
import sys

# Realizirati metodu ind_sum
def ind_sum(p):
    # Vaše izmjene:
    numbers = list(p)

    for index, num in enumerate(numbers):
        first_group = numbers[:index]
        second_group = numbers[index+1:]
        if sum(first_group) == sum(second_group):
            return index

if __name__ == '__main__':
    fout = open('izlaz.txt', 'w')
    fin = open('ulaz.txt', 'r')

    n = int(fin.readline())
    p = map(int, fin.readline().rstrip().split())

    x = ind_sum(p)

    fout.write(str(x) + '\n')
    fout.close()
    fin.close()



    


